import React from 'react';
import {Link} from 'react-router-dom';
import RedDot from "../components/RedDot";

/**
 * @name - 
 * @summary - 
 * @description - 
 * @prop - 
 */
export default (props) => {

  return (
    <div id="splashpage-container" className="page">
      
      <div id="splash-content-left" className="padded-content">
        <h3 id="splash-header"> MimbleWimble Wallet <br/> "Dobby" </h3>
        <span style={{color: "red"}}> Nothing changes, only rearranges </span>
      </div>
      
      <div style={{position: "absolute", left: "70%"} }className="vertical-line"/>
      
      <div id="splash-content-right">
        <RedDot radius="15vw" className="padded-content">
          <Link to="/login">
            <button className="red-dot-button"> Initiate Alchemy </button>
          </Link>
          
          <div style={{padding: "5%"}}/>

          <Link to="/import">
            <button className="red-dot-button"> Import Alchemy Recipe </button>
          </Link>
        </RedDot>
      </div>
    </div>
  )
}