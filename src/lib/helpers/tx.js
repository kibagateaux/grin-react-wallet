
const GRIN_BASE = 1000000000;

export const amountAsHr = (in_amount, sigdigs) => {
  const amount = in_amount / GRIN_BASE;
  return amount.toFixed(sigdigs);
}

export const hrToAmount = (in_amount) => {
  return Math.trunc(in_amount * GRIN_BASE);
}

export const netChange = (tx) => {
  if (tx == null) {
    return 0;
  }
  return tx.amount_credited - tx.amount_debited;
}

export const parseDate = (date_str) => {
  if (date_str === null) {
    return 'None';
  }
  return new Date(Date.parse(date_str)).toLocaleString();
}

export const parseConfirmed = (tx) => {
  if (tx == null) {
    return '';
  }
  if (tx.confirmed) {
    return 'Confirmed';
  } else {
    return 'Unconfirmed';
  }
}

export const parseTxType = (tx) => {
  if (tx == null) {
    return '';
  }
  switch (tx.tx_type) {
    case 'ConfirmedCoinbase' :
      return 'Coinbase';
    case 'TxReceived' :
      return 'Received';
    case 'TxSent' :
      return 'Sent';
    case 'TxReceivedCancelled' :
      return 'Cancelled Receive';
    case 'TxSentCancelled' :
      return 'Cancelled Send';
  }
  return 'Unknown';
}